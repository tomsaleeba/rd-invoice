#!/bin/bash
set -euxo pipefail
cd "$(dirname "$0")"

outDir=public

rm -fr $outDir/
mkdir $outDir/
# trying to include the Vue script gave syntax errors on the closing </script>
# tag, so we'll leave it separate.
cp ./vue.global.js $outDir/

function doAwk {
  awk \
    -f - \
    ./index.html \
    > $outDir/index.html
}

cat << HEREDOC | doAwk
/link href.*style/ {
  print "<style type=\"text/css\">"
  system("cat ./style.css");
  print "</style>"
  next;
};

/script\.js/ {
  print "<script type=\"text/javascript\">"
  system("cat ./script.js");
  print "</script>"
  next;
};

{
  print;
}
HEREDOC
