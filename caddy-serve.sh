#!/bin/bash
set -euxo pipefail
cd "$(dirname "$0")"

docker run --rm -it -p 8080:80 \
  -v $PWD/:/usr/share/caddy/ \
  caddy:2.6-alpine
