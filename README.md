A simple webpage that lets you build an invoice that looks "as expected" when
printed as a PDF. It's specifically for one company, but you can copy the code
and modify the items for your own business.

See live deployed app: [https://tomsaleeba.gitlab.io/rd-invoice/](https://tomsaleeba.gitlab.io/rd-invoice/).

Design features
- easy to add items to invoice; you can see all options (no `<select>`), just
  click the counts to set the quantity
- no need to submit the page or run a server, just add the items and the
  `print` media view will hide the input forms
- always fit on A4 paper, by setting a `max-width` on the `table` in real world
  units
- minimal duplication in code, easy to add new items

## Local dev
There is no build step or package manager needed to get going. It's just a
regular HTML file and the only dependency (VueJS) is in version control.
- ensure you have `docker` installed
- `./caddy-serve.sh`
- open [http://localhost:8080/](http://localhost:8080/)

## The build
To avoid issue with caching, the file app is a single HTML file with (almost)
everything in it. "Almost" because currently including the Vue script causes
syntax errors. The build is done with `./build.sh` as part of the CD pipeline.

It uses `awk` to find the lines that import scripts and styles, and replaces
them with in line wrapper tags and the contents of those files.
