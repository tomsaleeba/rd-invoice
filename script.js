const { createApp } = Vue

const lsKey = 'rd-invoice.common-details'
const taxRate = 0.1

createApp({
  data() {
    const result = {
      ...defaultYouAndInvoiceeDetails(),
      nextJobId: 1,
      nextCustomItemId: 1,
      jobs: [],
      isIncludeGst: false,
    }
    const savedDetails = localStorageGet()
    if (savedDetails) {
      result.you = {
        ...result.you,
        ...savedDetails.you,
      }
      result.invoicee = {
        ...result.invoicee,
        ...savedDetails.invoicee,
      }
    }
    return result
  },
  mounted() {
    startDetailsSaver(this)
  },
  computed: {
    jobTotals() {
      return this.jobs.map(currJob => {
        return this.allItemsFrom(currJob).reduce((accum, currItem) => {
          return accum + sumCostFor(currItem.entries)
        }, 0)
      })
    },
    grandTotalExTax() {
      return this.jobTotals.reduce((accum, curr) => accum + curr, 0)
    },
    gst() {
      return this.formatMoney(this.grandTotalExTax * taxRate)
    },
    grandTotal() {
      if (this.isIncludeGst) {
        return this.formatMoney(this.grandTotalExTax * (1 + taxRate))
      }
      return this.formatMoney(this.grandTotalExTax)
    },
  },
  methods: {
    formatMoney(val) {
      if (!val || Number.isInteger(val)) {
        return val
      }
      return val.toFixed(2)
    },
    addJob() {
      const jobDef = newJobDef(this.nextJobId)
      this.jobs.push(jobDef)
      this.nextJobId += 1
    },
    addCustomItemToJob(jobId) {
      const found = this.jobs.find(j => j.id === jobId)
      if (!found) {
        console.error(`Could not find job ${jobId}`)
        return
      }
      found.customItems.push(newCustomItemDef(this.nextCustomItemId, 1))
      this.nextCustomItemId += 1
    },
    decrementQuantity(e) {
      if (e.quantity === 0) {
        return
      }
      e.quantity -= 1
    },
    onLabelEdit(e) {
      if (e.quantity) {
        return
      }
      e.quantity = 1
    },
    allItemsFrom(job) {
      return [
        ...job.items,
        {
          title: 'Other',
          entries: job.customItems,
        }
      ]
    },
    removeCustomItem(job, customItem) {
      const index = job.customItems.findIndex(e => e.id === customItem.id)
      if (index < 0) {
        console.error(`Could not find custom item ID=${customItem.id} in job ID=${job.id}`)
        return
      }
      job.customItems.splice(index, 1)
    },
    clearYouAndInvoicee() {
      const defaultData = defaultYouAndInvoiceeDetails()
      this.you = defaultData.you
      this.invoicee = defaultData.invoicee
      doDetailsSave(this)
    },
    onDelete(index) {
      this.jobs.splice(index, 1)
    },
    onToggleCollapse(index) {
      this.jobs[index].isExpanded = !this.jobs[index].isExpanded
    },
    demoData() {
      this.you = {
        abn: '112233445566',
        name: 'Worky McWorkface',
        address: '456 Main St, SUBURB SA 5000',
        bankName: 'Some bank',
        bankAccName: 'Sir Worky McWorkface',
        bankBsb: '123-456',
        bankAccNum: '9987-654321',
      }
      this.invoicee = {
        date: '2022-11-30',
        invoiceNumber: 'INV-1234',
        abn: '9988667733',
        name: 'Big Boss',
        address: '999 Company Lane, Capitalistville SUBURB SA 5000',
      }
      this.addJob()
      ;(job => {
        job.date = '2022-11-22'
        job.address = '123 Something St, SUBURB SA 5001'
        job.name = 'Customer McBlahface'
        job.items.forEach(i => {
          i.entries[0].quantity = Math.ceil(Math.random() * 3)
          const notable = i.entries.filter(e => e.allowNotes)
          notable.forEach(e => {
            e.notes = 'some note'
            e.quantity = 1
          })
        })
        job.items.forEach(i => {
          i.entries[0].quantity = Math.ceil(Math.random() * 3)
          const notable = i.entries.filter(e => e.allowNotes)
          notable.forEach(e => e.notes = 'some note')
        })
        const aCustomItem = newCustomItemDef(99, 90)
        aCustomItem.label = 'Extra travel KMs'
        aCustomItem.cost = 1.30
        job.customItems.unshift(aCustomItem)
      })(this.jobs[this.jobs.length -1])
    },
    formatDate(dateStr) {
      const [year, month, day] = dateStr.split('-')
      return `${day}/${month}/${year}`
    },
  }
}).mount('#app')

function sumCostFor(theArray) {
  return theArray.reduce((accum, curr) => {
    if (!curr.quantity) {
      return accum
    }
    accum += curr.cost * curr.quantity
    return accum
  }, 0)
}

function rowsFor(theArray) {
  let result = theArray.filter(e => e.quantity).length
  if (result) {
    const theHeadingRow = 1
    result += theHeadingRow
  }
  return result
}

function newJobDef(id) {
  const result = {
    id: id,
    isExpanded: true,
    date: new Date().toISOString().substr(0,10),
    address: null,
    name: null,
    items: [
      {
        title: 'Series 1 Roller Doors',
        entries: [
          ...[
            {id: 2200, cost: 80},
            {id: 2500, cost: 95},
            {id: 2700, cost: 105},
            {id: 3000, cost: 115},
          ].map(e => ({
            ...e,
            label: `Up to ${e.id}mm`,
          })),
          {id: 'neo', label: 'NEO Add', cost: 35},
        ]
      }, {
        title: 'Series 2/3 Roller Doors',
        entries: [
          {id: '4400x2400', cost: 190},
          {id: '4400x3000', cost: 295},
          {id: '4400x3600', cost: 325},
          {id: '4400x5100', cost: 420},
          {id: '5100x2400', cost: 255},
          {id: '5100x3000', cost: 315},
          {id: '5100x3600', cost: 360},
          {id: '5100x5100', cost: 525},
          {id: '5500x2400', cost: 265},
          {id: '5500x3000', cost: 345},
          {id: '5500x3600', cost: 370},
          {id: '5500x5100', cost: 525},
        ].map(e => {
          const [width, height] = e.id.split('x')
          return {
            ...e,
            label: `Width: up to ${width}mm; Height: up to ${height}mm`,
          }
        })
      }, {
        title: 'PanelLifts (Includes fitting wall button & keypad if required)',
        entries: [
          {id: '2275x3050', cost: 235},
          {id: '2275x4705', cost: 275},
          {id: '2275x5585', cost: 315},
          {id: '2275x6600', cost: 400},
          {id: '2545x3050', cost: 275},
          {id: '2545x4705', cost: 315},
          {id: '2545x5585', cost: 370},
          {id: '2545x6600', cost: 420},
          {id: '3415x3050', cost: 350},
          {id: '3415x4705', cost: 380},
          {id: '3415x5585', cost: 475},
          {id: '3415x6600', cost: 525},
        ].map(e => {
          const [height, width] = e.id.split('x')
          return {
            ...e,
            label: `Height: up to ${height}mm; Width: up to ${width}mm`,
          }
        })
      }, {
        title: 'FlexaDoor',
        entries: [
          ...[
            {id: 2130, cost: 235},
            {id: 2790, cost: 265},
          ].map(e => ({
            ...e,
            label: `Up to ${e.id}mm`,
          })),
          {id: 'ret', label: 'Ret. Header', cost: 25},
        ],
      }, {
        title: 'Take down Roller Doors & FlexaDoors',
        entries: [
          ...[
            {id: 'up to', cost: 35},
            {id: 'over', cost: 65},
          ].map(e => ({
            ...e,
            label: `Doors ${e.id} 3600mm wide`,
          })),
          {id: 'dump', label: 'Dump', cost: 20},
        ],
      }, {
        title: 'Take down Tilt Doors, Swing Doors & PanelLift Doors',
        entries: [
          ...[
            {id: 'up to', cost: 55},
            {id: 'over', cost: 85},
          ].map(e => ({
            ...e,
            label: `Doors ${e.id} 4000mm wide`,
          })),
          {id: 'dump', label: 'Dump', cost: 20},
        ],
      }, {
        title: 'Panel replacements & Curtain kits (incl. t/down and dump)',
        entries: [
          ...[
            {id: 'Up to 3545;1', cost: 60},
            {id: 'Up to 3545;2', cost: 85},
            {id: 'Up to 3545;3', cost: 110},
            {id: 'Up to 3545;kits', cost: 160},
            {id: '3550-5285;1', cost: 75},
            {id: '3550-5285;2', cost: 105},
            {id: '3550-5285;3', cost: 150},
            {id: '3550-5285;kits', cost: 210},
            {id: '5290-6600;1', cost: 130},
            {id: '5290-6600;2', cost: 160},
            {id: '5290-6600;3', cost: 200},
            {id: '5290-6600;kits', cost: 315},
          ].map(e => {
            const [frag, panels] = e.id.split(';')
            const msg = panels === 'kits' ? `Kits` : `${panels} Panel(s)`
            return {
              ...e,
              label: `Width: ${frag}; ${msg}`,
            }
          }),
          {id: 'dump', label: 'Dump', cost: 20},
        ],
      }, {
        title: 'Additional motor installation charges',
        entries: [
          {id: 'rdn', label: 'Roller Door New', cost: 50},
          {id: 'rdr', label: 'Roller Door Retro', cost: 55},
          {id: 'pfn', label: 'Panelift/Flexadoor New', cost: 65},
          {id: 'pfr', label: 'Panelift/Flexadoor Retro inc Service', cost: 30},
        ],
      }, {
        title: 'General Accessories',
        entries: [
          {id: 'fws', label: 'Fitting Weather Seals (per pair)', cost: 20},
          ...[['Series 1', 20], ['Series 2', 25], ['Panel', 35]].map(([series, cost]) => ({
            id: `fes${series}`,
            label: `Fitting Emb-A-Seal (All Types, per pair); ${series}`,
            cost,
          })),
          {
            id: 'cad',
            label: 'Combo Access Device / External Access Device / Emergency (each)',
            cost: 35,
          }, {
            id: 'irb',
            label: 'Infra Red Beams (each)',
            cost: 35,
          }, {
            id: 'bba',
            label: 'Battery Back up (each)',
            cost: 15,
          }, {
            id: 'aul',
            label: 'Auto lock',
            cost: 20,
          }, {
            id: 'spk',
            label: 'Smart Phone Kit (incl. assiting customer)',
            cost: 20,
          }, {
            id: 'sbe',
            label: 'Shoot Bolts / Extra Lock (per pair)',
            cost: 45,
          },
        ]
      }, {
        title: 'Door related items',
        entries: [
          {id: 'mla', label: 'Manual Lock Assembly Panel Lift Doors (each)', cost: 25},
          ...[[1, 70], [2, 100]].map(([series, cost]) => ({
            id: `mul${series}`,
            label: `Mullions (per pair); Series ${series}`,
            cost,
          })),
          ...[[1, 45], [2, 75]].map(([series, cost]) => ({
            id: `hwg${series}`,
            label: `High Wind Guides (per pair); Series ${series}`,
            cost,
          })),
          ...[[2500, 85], [3000, 105]].map(([height, cost]) => ({
            id: `reh${height}`,
            label: `Re-Install roller door & motor up to H ${height}mm (inc service, per door)`,
            cost,
          })),
          ...[
            [2400, 5100, 130], [2400, 5500, 160],
            [3000, 5100, 2000], [3000, 5500, 210]
          ].map(([height, width, cost]) => ({
            id: `rew${height}${width}`,
            label: `Re-Install roller door & motor up to H ${height}mm x W ${width}mm (inc service, per door)`,
            cost,
          })),
          {id: 'cos', label: 'Change over panelift door springs (per spring)', cost: 45},
          ...[['single', 60], ['double', 90]].map(([size, cost]) => ({
            id: `spd${size}`,
            label: `Special Panelift doors (danmar, aluminium, timer, etc) - incl. crate; ${size}`,
            cost,
          })),
          {id: 'qck', label: 'Quick Closer kit', cost: 30},
          {id: 'tap', label: 'Taper (sectional door only)', cost: 20},
          ...[['Up to', 30], ['Over', 50]].map(([size, cost]) => ({
            id: `ins${size}`,
            label: `Insulshield (${size} 3600 W)`,
            cost,
          })),
        ]
      }, {
        title: 'Structural items',
        entries: [
          {id: 'tpa', label: 'Timber Packers (each)', cost: 20},
          {id: 'tpo', label: 'Timber Posts (each)', cost: 30},
          {id: 'fsp', label: 'Fixing Steel Plate (each)', cost: 15},
          {id: 'fst', label: 'Fixing Steel Tube Lengths (each)', cost: 20},
          {id: 'ffs', label: 'Fabricating & Fixing Steel Plate (each)', cost: 20},
          {id: 'fts', label: 'Fitting Timber in Ceiling (per fix)', cost: 30},
          {id: 'csp', label: 'C-Sections / Post - (All sizes, each)', cost: 40},
          {id: 'dig', label: 'Dig Hole (each)', cost: 25},
          {id: 'fll', label: 'Flashing - Large (each)', cost: 30},
          {id: 'fls', label: 'Flashing - Small (each)', cost: 20},
          {id: 'sid', label: 'Side panels - up to 1 metre (each)', cost: 70},
          {id: 'sie', label: 'Side panels - per metre after first (each)', cost: 20},
        ]
      }, {
        title: 'Re-works / Service / Warranty',
        entries: [
          {id: 'sca', label: 'Service Call (per site)', cost: 60},
          {id: 'iwi', label: 'Installer Warranty / Installation issues with-in 12 months', cost: 0},
          {id: 'iws', label: 'Installer Warranty / Sensitivity / Reversing / Spring Tension with-in 6 months', cost: 0},
        ]
      }, {
        title: 'Fall Overs / Extra Labour',
        entries: [
          {id: 'fov', label: 'Fall Overs / Wasted Trips (per job)', cost: 25},
          {id: 'lab', label: 'Labour (per hour)', cost: 70, allowNotes: true, notes: null},
        ]
      }
    ],
    customItems: [
      newCustomItemDef(0, 0),
    ],
  }
  result.items.forEach(i => i.entries.forEach(e => e.quantity = 0))
  return result
}

function newCustomItemDef(id, quantity) {
  return {
    id,
    cost: 1,
    label: 'edit me',
    quantity,
  }
}

function localStorageGet() {
  if (!localStorage) {
    return
  }
  const jsonStr = localStorage.getItem(lsKey)
  return JSON.parse(jsonStr)
}

function localStorageSet(data) {
  if (!localStorage) {
    return
  }
  const jsonStr = JSON.stringify(data)
  localStorage.setItem(lsKey, jsonStr)
}

function doDetailsSave(vmObj) {
  const data = {
    you: vmObj.you,
    invoicee: vmObj.invoicee,
  }
  localStorageSet(data)
}

function startDetailsSaver(vmObj) {
  setInterval(() => {
    doDetailsSave(vmObj)
  }, 5000)
}

function defaultYouAndInvoiceeDetails() {
  return {
    you: {
      abn: null,
      name: null,
      address: null,
      bankName: null,
      bankAccName: null,
      bankBsb: null,
      bankAccNum: null,
    },
    invoicee: {
      abn: null,
      name: null,
      address: null,
      date: '',
      invoiceNumber: null,
    },
  }
}
